import { useState, useEffect, useLayoutEffect } from 'react'
import { ethers } from 'ethers'
import Greeter from './artifacts/contracts/Greeter.sol/Greeter.json'

import { MetaMaskInpageProvider } from "@metamask/providers";

declare global {
  interface Window {
    ethereum: MetaMaskInpageProvider;
  }
}

export const App = () => {
  const [data, setData] = useState("")
  const [contract, setContract] = useState()

  const getData = async () => {

  }

  const updateData = async () => {
    const transaction = await contract.setGreeting(data)
    await transaction.wait()
    getData()
  }

  const initConnection = async () => {
    if (typeof window.ethereum !== "undefined") {
      await window.ethereum.request({ method: "eth_requestAccounts" })
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const signer = provider.getSigner()
      setContract(
        new ethers.Contract(
          "__Number_of_contract__",
          Greeter.abi,
          singner
        )
      )
    }else {
      console.log("Please install Metamask")
    }
  }

  useEffect(() => {
    initConnection()
  }, [])

  return (
    <div></div>
  )
}